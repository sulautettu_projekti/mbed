#include "mbed.h"
#include "LinearTempSensor.h"
#include "EthernetInterface.h"
#include "NTPClient.h"
#include "HTTPClient.h"
#include "math.h"
#include <string>

EthernetInterface eth;
HTTPClient http;
NTPClient ntp;
Serial pc(USBTX,USBRX);
int samples=10;
LinearTempSensor sensor(p20, samples, LinearTempSensor::MCP9700); // With option parameters(data_pin, avg_samples, sensor_type)
string sensor_id = "20";
string temp, timestamp = 0;
string error_timestamp;
bool eth_error = false;
void http_post(string,string,string,string);
string read_temp(), get_time();
string gateway;
float To, Tav, value, value2;

int main()
{
    eth.init();
        
    while(true)
    {        
        if (eth.connect()==0){                            //if connection ok
            gateway = eth.getGateway();
            pc.printf("\n\n\r-------\n\rConnected! MY IP Address is %s\n\r", eth.getIPAddress());
            pc.printf("\rgateway is %s", gateway);
            temp = read_temp();                           //get temp
            timestamp = get_time();                       //get time from ntp server
            
            
            if (temp == "ERROR:temp")
                http_post(timestamp, sensor_id, temp, "temp error");
                
            else if (timestamp == "0")
                http_post(timestamp, sensor_id, temp, "ntp error");
            
            else
                http_post(timestamp, sensor_id, temp, "");           //send data with HTTP
            
            eth_error=false;
            pc.printf("\n\rTemp: %s", temp);
            //pc.printf("\n\rTime epoch: %s", time);
            //pc.printf("\n\rsensor id: %s", sensor_id);
        }
        else{
            if (eth_error == false){
                eth_error = true;
                error_timestamp = timestamp;
                pc.printf("\n\rConnection problem..");
            }
        }
        eth.disconnect();
        wait(10.0);
    }
}



////////////GET TEMP/////////////
string read_temp()
{
int err_counter=0;
    for (int n=0; n<samples; n++)                   //Measures samples
    {
        value = sensor.Sense();                     //Get sample
        //printf("\n\rSample %i: %f", n, value);
        
        if (value > 1000 || value < 300){           //Value over acceptable range
            //printf(" Rejected!");
            err_counter++;
            n=0;
        }
        else if (n!=0 && ((value-value2)<-10 || (value-value2)>10)){
            //printf(" Error!");
            //printf(" value difference: %f", value-value2);
            n=0;
            err_counter++;
        }
        value2 = value;
        if (err_counter > 10)                       //Error counter > 10 get out for-loop
            n = samples;
            
        wait(0.5);                                  //Delay between samples
    }
    
    
    if (err_counter > 10)                           //Error counter > 10 return error temperature
        return "ERROR:temp";
    else{
        Tav = sensor.GetAverageTemp();                          // Calculate average temperature from N samples
        //To = sensor.GetLatestTemp();                          // Calculate temperature from the latest sample
        double rounded = floor((Tav+0.05)*10)/10;               // Round temperature
        
        //pc.printf("\n\rTemp(avg):%f  Temp:%f", Tav, To);
        //pc.printf("\n\rTemp(avg):%f  rounded: %f", Tav, rounded);
        char buffer[1];
        sprintf(buffer, "%f", rounded);
        string s_temp = buffer;
        return s_temp;
    }
}


////////////GET TIME////////////
string get_time()
{
    time_t ctTime;
    //pc.printf("\n\n\rTrying to update time...");
    if (ntp.setTime(gateway.c_str()) == 0)
    {
      ctTime = time(NULL);
      pc.printf("\nSet time successfully");
      //pc.printf("\n\rTime is set to (UTC): %s", ctime(&ctTime));
    }
    else
    {
      ctTime = 0;
      pc.printf("\nError... Time set to 0!");
    }
    
    char time[20];
    sprintf(time, "%d", ctTime);
    return time;
}



//////////POST DATA//////////
void http_post(string time, string id, string temp, string error)
{
    char str[512];
    HTTPMap map;
    HTTPText inText(str, 512);
    
    const char* time_char = time.c_str();
    const char* id_char = id.c_str();
    const char* temp_char = temp.c_str();
    
    if(error=="") {
        map.put("timestamp", time_char);
        map.put("sensorId", id_char);
        map.put("temperature", temp_char);
    }
    
    else if(error=="temp error"){
        map.put("ERROR", "TEMP");
        map.put("MESSAGE", ("temp error @ timestamp " + time + " in sensor " + id).c_str());
        map.put("timestamp", time_char);
        map.put("sensorId", id_char);
        map.put("temperature", "999");
    }
    
    else if(error=="ntp error"){
        map.put("ERROR", "NTP");
        map.put("MESSAGE", "ntp error");
        map.put("timestamp", time_char);
        map.put("sensorId", id_char);
        map.put("temperature", temp_char);
    }
    
    else if(error=="" && eth_error==true){
        map.put("timestamp", time_char);
        map.put("sensorId", id_char);
        map.put("temperature", temp_char);
        map.put("ERROR", "ETHERNET");
        map.put("MESSAGE", ("ethernet error @ timestamp " + error_timestamp).c_str());
    }
    
    //pc.printf("\n\rTrying to post data...");
    int ret = http.post(("http://" + gateway + "/sensor.py").c_str(), map, &inText, 1000);
    if (!ret)
    {
      pc.printf("\n\rExecuted POST successfully - read %d characters", strlen(str));
      pc.printf("\n\rResult: %s", str);
    }
    else
    {
      pc.printf("\n\rError - ret = %d - HTTP return code = %d", ret, http.getHTTPResponseCode());
    }

}