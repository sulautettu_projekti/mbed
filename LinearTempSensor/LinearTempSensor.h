/* mbed Linear Temperature Sensor library
 * Supports Microchip MCP9700/9701, National Semiconductor LM35
 *
 * Written by Todotani, Nov 22 2010
 */
 
#ifndef MBED_LINEARTEMPSENSOR_H
#define MBED_LINEARTEMPSENSOR_H

#include "mbed.h"

/** Linear Temperature Sensor class.
 * Sample and store sensor acuired value in N (default=10) times and
 * calculate avarage temperature from sampled data
 * Supports Microchip MCP9700/9701, National Semiconductor LM35
 * @author Todotani
 */
class LinearTempSensor {
public:
    /** Sensor Type Definitions  */
    enum SensorType {
        MCP9700,    /**< Microchip MCP9700 (Default)  */
        MCP9701,    /**< Microchip MCP9701 */
        LM35        /**< National Semiconductor LM35 */
    };
    
    /** Create a Temperature Sensor instanse
     *
     * @param ain       PinName of analog input
     * @param N         Number of samples to calculate average temperature (default = 10)
     * @param type      Sensor type (default = MCP9700)
     */
    LinearTempSensor(PinName ain, int N = 10, SensorType type = MCP9700);
    
    /** Sample (read) sensor data and store to buffer
     *
     * @param None
     * @return Sensor-acuired value (mV)
     */
     float Sense();
     
    /** Calculate average temperature from sample buffer
     *
     * @param None
     * @return Average temperature from N times of sumple (Centigrade)
     */
     float GetAverageTemp();
     
    /** Calculate temperature from the latest sample
     *
     * @param None
     * @return Temperature from the latest sampled data (Centigrade)
     */
     float GetLatestTemp();

     ~LinearTempSensor();

private:
    AnalogIn    _ain;
    int         _samples;
    SensorType  _type;
    
    float       *sampleBuffer;      // Buffer to store sensor acuired data
    bool        bufferNotFilled;    // Flag shows that buffer have not filled
    uint32_t    sampleCount;
    uint32_t    index;

    float   V0;                      // Sensor read value in case 0 degree
    float   Tc;                      // Tmperature coefficient (temprature inclease) in each degree
    float   Vref;                    // Reference volgate for ADC
};

#endif